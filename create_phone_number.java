import java.util.Arrays;
import java.util.stream.*;

public class Kata {
  public static String createPhoneNumber(int[] numbers) {
    // Your code here!
     
    StringBuilder sb = new StringBuilder(Arrays.stream(numbers)
                                               .mapToObj(i -> ((Integer) i).toString())
                                               .collect(Collectors.joining()));
    sb.insert(6, '-');
    sb.insert(3, ") ");
    sb.insert(0, '(');
    
    return sb.toString();
  }
}